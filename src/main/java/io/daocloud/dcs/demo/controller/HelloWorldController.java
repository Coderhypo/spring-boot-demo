package io.daocloud.dcs.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloWorldController {

    @ResponseBody
    @RequestMapping("/")
    public String hello() {
        return "<h1>Hello World!<sub><small>v1.0</small></sub>";

    }
}
